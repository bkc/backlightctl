package main

import (
	"flag"
	"gitlab.com/bkc/backlightctl/internal/backlight"
	"gitlab.com/bkc/backlightctl/internal/service"
	"log"
	"net"
	"os"

	proto "gitlab.com/bkc/backlightctl/proto/go"

	"google.golang.org/grpc"
)

var acpiPath = flag.String("acpi-path", "/sys/class/backlight/intel_backlight", "The <path> to where the backlight is controlled")

func main() {
	flag.Parse()

	os.Remove("/var/run/backlightd.sock")

	listener, err := net.Listen("unix", "/var/run/backlightd.sock")
	if err != nil {
		log.Fatalf("net.Listen: %v", err)
	}
	defer func(l net.Listener) {
		l.Close()
		os.Remove("/var/run/backlightd.sock")
	}(listener)
	os.Chmod("/var/run/backlightd.sock", 0666)

	server := grpc.NewServer([]grpc.ServerOption{}...)

	proto.RegisterBrightnessServiceServer(server, &service.BacklightService{Backer: &backlight.IntelBacklight{}})

	log.Println("starting server...")
	log.Println(server.Serve(listener))
}
