package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	"gitlab.com/bkc/backlightctl/internal/backlight"
)

var set = flag.Int("set", 0, "Set a value. 0 means don't set it...")
var max = flag.Bool("max", false, "Get max")
var up = flag.Bool("up", false, "Increase backlight by 1 step")
var down = flag.Bool("down", false, "Decrease backlight by 1 step")

var socket = flag.String("socket", "unix:///var/run/backlightd.sock", "Socket to connect to. Has to be a valid URI")

func errCheck(err error, prefix string) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %s: %v\n", prefix, err)
		os.Exit(1)
	}
}

func main() {
	flag.Parse()

	bl, err := backlight.NewGrpcBacklight(*socket, 2*time.Second)
	errCheck(err, "can not connect to daemon")

	if *up {
		curr, err := bl.Current()
		errCheck(err, "can not get current")
		max, err := bl.Max()
		errCheck(err, "can not get max")
		currMap := mapit(curr, max)
		if currMap == len(logMap)-1 {
			fmt.Println("already set to max")
			os.Exit(0)
		}
		errCheck(bl.Set(getVal(currMap+1, max)), "can not set current")
	}
	if *down {
		curr, err := bl.Current()
		errCheck(err, "can not get current")
		max, err := bl.Max()
		errCheck(err, "can not get max")
		currMap := mapit(curr, max)
		if currMap == 0 {
			fmt.Println("already set to min")
			os.Exit(0)
		}
		errCheck(bl.Set(getVal(currMap-1, max)), "can not set current")
	}

	if *set > 0 {
		err := bl.Set(int32(*set))
		errCheck(err, "can not set current")
	}
	if *max {
		curr, err := bl.Max()
		errCheck(err, "can not get max")
		fmt.Printf("max: %d\n", curr)
	} else {
		curr, err := bl.Current()
		errCheck(err, "can not get current")
		fmt.Printf("current: %d\n", curr)
	}
}

var logMap = []int32{0, 1, 2, 4, 6, 10, 16, 25, 40, 63, 100}

func mapit(curr, max int32) int {
	step := max / 100
	c := curr / step
	for i := range logMap {
		if logMap[i] == c {
			return i
		}
	}
	return len(logMap) - 1 // Just return max...
}

func getVal(off int, max int32) int32 {
	step := max / 100
	return logMap[off] * step
}
