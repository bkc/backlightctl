# backlightctl

Daemon for ACPI-based backlight control

# building

```
go build -o ./backlightctl ./cmd/backlightctl
go build -o ./backlightd ./cmd/backlightd
```

# regenerate protobuf bindings
you'll need `protoc` installed AND `github.com/golang/protobuf/protoc-gen-go` installed
```
rm ./proto/go/*.pb.go
protoc  --go_out=paths=source_relative,plugins=grpc:./proto/go/ ./proto/backlight.proto
```