package backlight

// Backlight defines the methods requires to control a backlight
type Backlight interface {
	Max() (int32, error)
	Current() (int32, error)
	Set(int32) error
}
