package backlight

import (
	"fmt"
	"io"
	"os"
	"strconv"
)

const intelPath = "/sys/class/backlight/intel_backlight"

type IntelBacklight struct {
	max int32
	cur int32
}

func (i *IntelBacklight) Current() (int32, error) {
	fp, err := os.Open(fmt.Sprintf("%s/brightness", intelPath))
	if err != nil {
		return 0, fmt.Errorf("could not open file: %v", err)
	}
	defer fp.Close()

	buf := make([]byte, 16)
	n, err := fp.Read(buf)
	if err != nil && err != io.EOF {
		return 0, fmt.Errorf("could not read file: %v", err)
	}
	if n == 0 {
		return 0, fmt.Errorf("read 0 bytes from file")
	}
	if buf[n-1] == byte('\n') {
		n--
	}

	val, err := strconv.ParseInt(string(buf[:n]), 10, 32)
	if err != nil {
		return 0, fmt.Errorf("could not parse integer: %v", err)
	}

	i.cur = int32(val)

	return int32(val), nil
}

func (i *IntelBacklight) Max() (int32, error) {
	fp, err := os.Open(fmt.Sprintf("%s/max_brightness", intelPath))
	if err != nil {
		return 0, fmt.Errorf("could not open file: %v", err)
	}
	defer fp.Close()

	buf := make([]byte, 16)
	n, err := fp.Read(buf)
	if err != nil && err != io.EOF {
		return 0, fmt.Errorf("could not read file: %v", err)
	}
	if n == 0 {
		return 0, fmt.Errorf("read 0 bytes from file")
	}
	if buf[n-1] == byte('\n') {
		n--
	}

	val, err := strconv.ParseInt(string(buf[:n]), 10, 32)
	if err != nil {
		return 0, fmt.Errorf("could not parse integer: %v", err)
	}

	i.max = int32(val)

	return int32(val), nil
}

func (i *IntelBacklight) Set(newVal int32) error {
	if i.max == 0 {
		_, err := i.Max()
		if err != nil {
			return err
		}
	}
	if newVal < 0 {
		return fmt.Errorf("can not set brightness lower than 0: %d", newVal)
	}
	if i.max < newVal {
		return fmt.Errorf("can not set brightness higher than max: %d > %d", newVal, i.max)
	}

	fp, err := os.OpenFile(fmt.Sprintf("%s/brightness", intelPath), os.O_WRONLY, 0644)
	if err != nil {
		return fmt.Errorf("could not open file: %v", err)
	}
	defer fp.Close()

	n, err := fmt.Fprintf(fp, "%d", newVal)
	if err != nil {
		return fmt.Errorf("could not write to file: %v", err)
	}
	if n == 0 {
		return fmt.Errorf("wrote 0 bytes to file")
	}

	i.cur = newVal

	return nil
}
