package backlight

import (
	"context"
	"fmt"
	"net"
	"strings"
	"time"

	proto "gitlab.com/bkc/backlightctl/proto/go"

	"google.golang.org/grpc"
)

// NewGrpcBacklight connects to the backlight grpc-daemon and returns a Backlight
//
// `addr` has to be a full valid URI with the scheme.
//  e.g. "unix:///var/run/backlightd.sock"
func NewGrpcBacklight(addr string, connTimeout time.Duration) (Backlight, error) {
	toCtx, toCancel := context.WithTimeout(context.Background(), connTimeout)
	defer toCancel()

	conn, err := grpc.DialContext(toCtx, addr, grpc.WithInsecure(), grpc.WithBlock(),
		grpc.WithContextDialer(func(ctx context.Context, addr string) (net.Conn, error) {
			s := strings.SplitN(addr, "://", 2)
			if len(s) != 2 {
				return nil, fmt.Errorf("not a valid unix-socket: %q", addr)
			}
			d := net.Dialer{}
			return d.DialContext(ctx, s[0], s[1])
		}))

	if err != nil {
		return nil, err
	}
	return &GrpcBacklight{
		client: proto.NewBrightnessServiceClient(conn),
	}, nil
}

type GrpcBacklight struct {
	client proto.BrightnessServiceClient
}

func (i *GrpcBacklight) Current() (int32, error) {
	ctx := context.Background()

	curr, err := i.client.GetCurrent(ctx, &proto.GetCurrentRequest{})
	if err != nil {
		return 0, fmt.Errorf("GrpcBacklight.Current: %v", err)
	}

	return int32(curr.GetBrightness()), nil
}

func (i *GrpcBacklight) Max() (int32, error) {
	ctx := context.Background()

	curr, err := i.client.GetMax(ctx, &proto.GetMaxRequest{})
	if err != nil {
		return 0, fmt.Errorf("GrpcBacklight.Max: %v", err)
	}

	return int32(curr.GetBrightness()), nil
}

func (i *GrpcBacklight) Set(newVal int32) error {
	ctx := context.Background()

	_, err := i.client.SetCurrent(ctx, &proto.SetCurrentRequest{Brightness: newVal})
	if err != nil {
		return fmt.Errorf("Grpc.Set: %v", err)
	}
	return nil
}
