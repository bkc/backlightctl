package service

import (
	"gitlab.com/bkc/backlightctl/internal/backlight"
)

// BacklightService defines the Backlight Service
type BacklightService struct {
	Backer backlight.Backlight
}
