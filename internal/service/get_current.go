package service

import (
	"context"

	proto "gitlab.com/bkc/backlightctl/proto/go"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// GetCurrent brightness
func (s *BacklightService) GetCurrent(ctx context.Context, rq *proto.GetCurrentRequest) (*proto.GetCurrentResponse, error) {
	i, err := s.Backer.Current()
	if err != nil {
		return nil, status.Errorf(codes.Internal, "Backlight.Current: %v", err)
	}
	return &proto.GetCurrentResponse{Brightness: int32(i)}, nil
}
