package service

import (
	"context"

	proto "gitlab.com/bkc/backlightctl/proto/go"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// GetMax brightness
func (s *BacklightService) GetMax(ctx context.Context, rq *proto.GetMaxRequest) (*proto.GetMaxResponse, error) {
	i, err := s.Backer.Max()
	if err != nil {
		return nil, status.Errorf(codes.Internal, "Backlight.Max: %v", err)
	}
	return &proto.GetMaxResponse{Brightness: int32(i)}, nil
}
