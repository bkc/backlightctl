package service

import (
	"context"
	"strings"

	proto "gitlab.com/bkc/backlightctl/proto/go"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// SetCurrent brightness
func (s *BacklightService) SetCurrent(ctx context.Context, rq *proto.SetCurrentRequest) (*proto.SetCurrentResponse, error) {
	err := s.Backer.Set(rq.GetBrightness())
	if err != nil {
		if strings.Contains(err.Error(), "can not set brightness") {
			return nil, status.Errorf(codes.FailedPrecondition, "Backlight.SetCurrent: %v", err)
		}
		return nil, status.Errorf(codes.Internal, "Backlight.SetCurrent: %v", err)
	}
	return &proto.SetCurrentResponse{}, nil
}
